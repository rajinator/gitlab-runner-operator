/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"fmt"
	"os"

	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	appsv1beta2 "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/api/v1beta2"
	"gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers"
	// +kubebuilder:scaffold:imports
)

var (
	scheme       = runtime.NewScheme()
	setupLog     = ctrl.Log.WithName("setup")
	buildVersion string
	buildDate    string
)

const (
	webhookCertDir  = "/apiserver.local.config/certificates"
	webhookCertName = "apiserver.crt"
	webhookKeyName  = "apiserver.key"
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(appsv1beta2.AddToScheme(scheme))
	// +kubebuilder:scaffold:scheme
}

func main() {
	var version bool
	var metricsAddr string
	var enableLeaderElection bool
	flag.BoolVar(&version, "version", false, "Prints the build version of the manager")
	flag.StringVar(&metricsAddr, "metrics-addr", ":8080", "The address the metric endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "enable-leader-election", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")

	flag.Parse()

	if version {
		fmt.Printf("GitLab Runner Operator Version: %s (%s)\n", buildVersion, buildDate)
		return
	}

	ctrl.SetLogger(zap.New(zap.UseDevMode(true)))

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:             scheme,
		MetricsBindAddress: metricsAddr,
		Port:               9443,
		LeaderElection:     enableLeaderElection,
		LeaderElectionID:   "b43e192d.gitlab.com",
		Namespace:          getWatchedNamespace(),
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	// Compile-time check that we fulfil the Client interface
	// This will guarantee that our mocks are interface are up-to-date with the
	// controller-runtime library.
	var reconcilerClient client.Client = controllers.NewRunnerReconcilerClient(mgr.GetClient())

	if err = (&controllers.RunnerReconciler{
		Client: reconcilerClient,
		Log:    ctrl.Log.WithName("controllers").WithName("Runner"),
		Scheme: mgr.GetScheme(),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Runner")
		os.Exit(1)
	}
	if os.Getenv("ENABLE_WEBHOOKS") != "false" {
		if err = (&appsv1beta2.Runner{}).SetupWebhookWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create webhook", "webhook", "Runner")
			os.Exit(1)
		}
	}
	// +kubebuilder:scaffold:builder

	// fix to use the OLM provided Certificate
	if isOLMCertPresent() {
		webhookSrv := mgr.GetWebhookServer()
		webhookSrv.CertDir = webhookCertDir
		webhookSrv.CertName = webhookCertName
		webhookSrv.KeyName = webhookKeyName
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}

func getWatchedNamespace() string {
	ns, _ := os.LookupEnv("WATCH_NAMESPACES")
	return ns
}

func isOLMCertPresent() bool {
	_, err := os.Stat(webhookCertDir)
	return err == nil
}
